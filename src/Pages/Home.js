import React, { Fragment } from 'react';

const Home = () => {
    return (
        <Fragment>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="jumbotron">
                            <h1 className="heading-4">Heading</h1>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius hic odio quo possimus! Nesciunt temporibus modi quo. Blanditiis, sed tempora?</p>
                            <div className="btn btn-outline-dark">Click</div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment >
    )
}

export default Home;
