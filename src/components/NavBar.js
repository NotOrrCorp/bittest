import React, { Fragment } from 'react';
// import { Container, Image, Menu, Dropdown, } from 'semantic-ui-react';

import logo from '../logo.svg';

const NavBar = () => {

    return (
        <Fragment>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="/"> <img src={ logo } alt="logo" height="30px" /> Jira Sample Page </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active"><a className="nav-link" href="/">Home <span className="sr-only">(current)</span></a></li>
                        <li className="nav-item"><a className="nav-link" href="/">Link</a></li>
                    </ul>
                </div>
            </nav>
        </Fragment>
    )
}

export default NavBar;
